<?php 
  
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../connect/Database.php';
  include_once '../models/Fish.php';

  // DB CONNECT
  $database = new Database();
  $db = $database->connect();
  $fish = new Fish($db);

  // FISH QUERY
  $result = $fish->fetchFish();
  $num = $result->rowCount();

  if($num > 0) {
    // FISH ARRAY
    $fish_arr = [];
    $fish_arr["Fish"] = array();
    $fish_arr["Count"] = $num;

    while($row = $result->fetch(PDO::FETCH_ASSOC)) {
     // var_dump($row);
      extract($row);

      $fish = array (
        'Pond' => 'ituli',
        'id' => $id,
        'Name' => $name,
        'Tank size' => $tank_size,
        'Number of fins' => $number_of_fins,
        'Time' => $created_at
    );

    $fish1 = array (
        'Pond' => 'amazon',
        'id' => $id,
        'pond id' => $amazon_id,
        'Name' => $fish_type,
        'Number of fins' => $amazon_number_of_fins,
        'Tank size' => $amazon_tank_size,
        'Time' => $created_at
    );
    
    // DETAMINE DATA TO SHOW
if ($amazon_id) {
    array_push($fish_arr["Fish"], $fish1);
  } else {
    array_push($fish_arr["Fish"], $fish);
  }

    }

    // OUTPUT JSON
    echo json_encode($fish_arr);

  } else {
    
    echo json_encode(
      array_merge([
        'success' => 1,
        'status' => 404,
        'message' => 'No Post Found'
    ])

    );
  }