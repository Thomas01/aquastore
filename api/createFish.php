<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

  include_once '../connect/Database.php';
  include_once '../models/Fish.php';

  // DB CONNECT
  $database = new Database();
  $db = $database->connect();

  // INSTANTIATE OBJECT
  $fish = new Fish($db);

  //CUSTOM ERROR MESSAGE
  function msg($success,$status,$message,$extra = []){
    return array_merge([
        'success' => $success,
        'status' => $status,
        'message' => $message
    ],$extra);
}
  
// GET DATA FORM REQUEST
$data = json_decode(file_get_contents("php://input"));
//var_dump($data);
$returnMessage = [];

// IF REQUEST METHOD IS NOT POST
if ($_SERVER["REQUEST_METHOD"] != "POST") {
    $returnMessage = msg(0,404,'Page Not Found!');
}

else if (!isset($data->name) 
    || !isset($data->number_of_fins) 
    || !isset($data->tank_size) 
    || empty(trim($data->name))
    || empty(trim($data->tank_size))
    || empty(trim($data->number_of_fins))
    )
    {

        $fields = ['fields' => ['name','number_of_fins','tank_size']];
        $returnMessage = msg(0,422,'Please Fill in all Required Fields!',$fields);
    
    }

    else
    
    {

      $fish->name = $data->name;
      //var_dump($fish->name);
      $fish->number_of_fins = $data->number_of_fins;
      $fish->tank_size = $data->tank_size;
      $fish->amazon_id = $data->amazon_id;
     
     
      $fish->createFish();
    }
  
      echo json_encode($returnMessage);

