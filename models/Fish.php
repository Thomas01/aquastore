<?php 
  class Fish {
    // db stuff
    private $conn;
    private $table = 'ituli';

    public $id;
    public $name;
    public $amazon_id;
    public $amazon_name;
    public $amazon_number_of_fins;
    public $amazon_tank_size;
    public $number_of_fins;
    public $tank_size;

    
    public function __construct($db) {
      $this->conn = $db;
    }

    public function msg($success,$status,$message,$extra = []){
      return array_merge([
          'success' => $success,
          'status' => $status,
          'message' => $message
      ],$extra);
  }

    // FETCH ALL FISH
    public function fetchFish() {
      $query = 'SELECT a.amazon_name as fish_type, a.id, a.amazon_id, i.name, i.id,
            i.tank_size, a.amazon_number_of_fins, i.number_of_fins, a.amazon_tank_size, i.created_at
                                    FROM ' . $this->table . ' i
                                    LEFT JOIN
                                        amazon a ON a.amazon_id = i.id
                                    ORDER BY
                                        created_at DESC';
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            return $stmt;
    }

    // Create Post
    public function createFish() {
          
      $name = trim($this->name);
      $number = trim($this->number_of_fins);
      $tank_size = trim($this->tank_size);
      $amazon_id = trim($this->amazon_id);

      // SET VALUEBLES
      $number_limit = 3;
      $tank = 20;// 20 gallons 
      $fish = "Goldfish";
      $fish1 = "guppy";

    if ( $fish1 !== $name && $number < $number_limit ) {

         try {

            $query = 'INSERT INTO ' . $this->table . ' SET name = :name, tank_size = :tank_size, number_of_fins = :number_of_fins';
            $stmt = $this->conn->prepare($query);

            // DATA BINDING
            $stmt->bindValue(':name', htmlspecialchars(strip_tags($name)),PDO::PARAM_STR);
            $stmt->bindValue(':number_of_fins', htmlspecialchars(strip_tags($number)),PDO::PARAM_STR);
            $stmt->bindValue(':tank_size', htmlspecialchars(strip_tags($tank_size)),PDO::PARAM_STR);
            // $stmt->bindParam(':department_id', $this->department_id);
            $stmt->execute();
            $returnMessage = msg(1,201,'You have successfully added ituli.');

         } catch (PDOException $e) {
            $returnMessage = msg(0,500,$e->getMessage());
         }

         echo json_encode($returnMessage);

     } else if ( $fish1 === $name && $tank_size > $tank ) {
          
          try {
            
                //SEARCH FOR GODFISH IN THAT TABLE AMAZOM
              $query = "SELECT name FROM ituli WHERE name =:name";
              $stmt = $this->conn->prepare($query);
              $stmt->bindValue(':name', htmlspecialchars(strip_tags($fish)),PDO::PARAM_STR);
              $stmt->execute();
          
          //$num = $stmt->rowCount();
          //var_dump($num);
          if ($stmt->rowCount()):
              //var_dump($stmt);
              $insert_query = "INSERT INTO `amazon`(`amazon_name`,`amazon_number_of_fins`,`amazon_tank_size`,`amazon_id`) VALUES(:name,:number_of_fins, :tank_size, :amazon_id)";
              $insert_stmt = $this->conn->prepare($insert_query);

              // DATA BINDING
              $insert_stmt->bindValue(':name', htmlspecialchars(strip_tags($name)),PDO::PARAM_STR);
              $insert_stmt->bindValue(':number_of_fins',  htmlspecialchars(strip_tags($number)),PDO::PARAM_STR);
              $insert_stmt->bindValue(':tank_size',  htmlspecialchars(strip_tags($tank_size)),PDO::PARAM_STR);
              $insert_stmt->bindValue(':amazon_id',  htmlspecialchars(strip_tags($amazon_id)),PDO::PARAM_STR);

              $insert_stmt->execute();
              $returnMessage = msg(1,201,'You have successfully added amazon.');
              echo json_encode($returnMessage);

          else:
              
              $query = 'INSERT INTO ' . $this->table . ' SET name = :name, tank_size = :tank_size, number_of_fins = :number_of_fins';
              $stmt = $this->conn->prepare($query);

              // DATA BINDING
              $stmt->bindValue(':name', htmlspecialchars(strip_tags($name)),PDO::PARAM_STR);
              $stmt->bindValue(':number_of_fins', htmlspecialchars(strip_tags($number)),PDO::PARAM_STR);
              $stmt->bindValue(':tank_size', htmlspecialchars(strip_tags($tank_size)),PDO::PARAM_STR);
              // $stmt->bindParam(':department_id', $this->department_id);
              $stmt->execute();
              $returnMessage = msg(1,201,'You have successfully added ituli.');
              echo json_encode($returnMessage);

          endif;

          } catch (PDOException $e) {
        
            $returnMessage = msg(0,500,$e->getMessage());
          }
          
          } else {

            try {

              $insert_query = "INSERT INTO `amazon`(`amazon_name`,`amazon_number_of_fins`,`amazon_tank_size`,`amazon_id`) VALUES(:name,:number_of_fins, :tank_size, :amazon_id)";
              $insert_stmt = $this->conn->prepare($insert_query);

              // DATA BINDING
              $insert_stmt->bindValue(':name', htmlspecialchars(strip_tags($name)),PDO::PARAM_STR);
              $insert_stmt->bindValue(':number_of_fins',  htmlspecialchars(strip_tags($number)),PDO::PARAM_STR);
              $insert_stmt->bindValue(':tank_size',  htmlspecialchars(strip_tags($tank_size)),PDO::PARAM_STR);
              $insert_stmt->bindValue(':amazon_id',  htmlspecialchars(strip_tags($amazon_id)),PDO::PARAM_STR);

              $insert_stmt->execute();
              $returnMessage = msg(1,201,'You have successfully added amazon.');
              echo json_encode($returnMessage);
      } catch (PDOException $e) {
             $returnMessage = msg(0,500,$e->getMessage());
      }

     }

   
      }
    
     
     
  }