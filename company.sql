CREATE TABLE `ituli` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
   `tank_size` int(25) NOT NULL,
   `number_of_fins` int(30) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);


INSERT INTO `ituli` (`id`, `name`, `tank_size`,`number_of_fins`) VALUES
(1, 'guppy','25','5'),
(2, 'catfish','30','4'),
(3, 'Anchovy','40','5'), 
(4, 'Angler catfish','40','7'),
(5, 'Anglerfish'46,6)


CREATE TABLE `amazon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amazon_id` int(17) NOT NULL,
  `amazon_name` varchar(255) NOT NULL,
  `amazon_number_of_fins` int(50) NOT NULL,
  `amazon_tank_size` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

INSERT INTO `amazon` (`id`, `table_amazon_id`, `amazon_name`, `amazon_number_of_fins`, `amazon_tank_size`) VALUES
(1, 1, 'Goldfish', '5','58'),
(2, 2, 'catfish', '7','60'),
(3, 1, 'Angler','6','58'),
(4, 4, 'Anglerfish', '4','34'),
(5, 4, 'Angler catfish', '6','28')
